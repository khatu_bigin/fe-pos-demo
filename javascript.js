

const renderMenu = (data) => {
    document.getElementById("menu").innerHTML = data
      .map(
        (value) => `
        <div>
        <ui class="menu-product" id="items-menu" ${value.id} " onclick="handleMenuItemClick(${value.id})">
            <li class="menu-a"${value.id }> <a href="#"><img src="${value.thumbnail}">${value.name}</a></li>
        </ui>
    </div>
        `
      )
  };
  
  const renderProducts = (data) => {
    document.getElementById("products").innerHTML = data
      .map(
        (value) => `
        <div class="row-col" ${value.id}>
            <div class="col-4 name-product">
                <img src="${value.thumbnail}">
                <div class="infor">
                    <h4 class="name">${value.name}</h4>
                    <p class="pr-price">${value.base_price_str} vnđ</p>
            </div>
        </div>
      `
      )
  };
  
  const main = async () => {
    data = await fetch("data.js")
      .then((response) => response.json())
      .then((data) => data);
  
    menu = data.map((item) => ({
      id: item.id,
      name: item.name,
      thumbnail: item.thumbnail,
    }));
  
    products = data.reduce(
      (acc, cur) => [
        ...acc,
        ...cur.products.map((p) => ({
          id: p.id,
          menu_id: cur.id,
          name: p.name,
          base_price: p.base_price,
          base_price_str: p.base_price_str,
          slug: p.slug,
          thumbnail: p.thumbnail,
        })),
      ],
      []
    );
  
    renderMenu(menu);
    renderProducts(products);
  
  };
